Capabilities and Requirements
=============================

* C1: Applications are able to access data from different sources (cities, communities, vertical solutions). 

  * R1: A uniform interface should be used; the context management API

  * R2: Information from all sources should use the same concepts, so called data information models

* C2: Applications are able to use both current and historical data, use geospatial querying and be automatically updated when the source data changes.

  * R3: The uniform interface should support retrieval of current data

  * R4: The uniform interface should support retrieval of historical data

  * R5: The uniform interface should support geospatial queryingç

  * R6: The uniform interface should support subscription to changes

* C3: Applications can discover and retrieve data relevant to their context from a variety of sources, including from within larger data sets

  * R7: Relevant data sources to any required context (at least location and time period) should be discoverable and retrievable according to their context

  * R8: Specific subsets of data relevant to the context should be retrievable from within larger data sets and with default limits and page sizes