Objectives
==========

* To enable information from different systems within a city or community (energy, mobility, education & skills etc.,) and from IoT devices and other data sources, to be brought together using a uniform interface. 
* To enable comprehensive and integrated use, sharing, and management of that data.
