# MIM1


## Getting started

Welcome to the repository for MIM 1: Context

The folder structure in this repository mirrors the structure of the Y.MIM specification:

* **Objectives** contains the objectives of the MIM
* **Capabilities and Requirements** contains the Capabilities and the Requirements to satisfy them
* **Mechanisms ans Specifications** contains subfolders, each of which details a Mechanism to implement the MIM and the specifications and building blocks required for its implementation
* **Compliance and Conformance** contains specs and tools that test an implementation's compliance with the MIM
* **Interoperability Guidance** contains practices, tools and examples of how interoperability between different Mechanisms may be achieved




